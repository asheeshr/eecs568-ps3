function [Li] = da_jcbb(z, ~)
% perform joint-compatability branch and bound data association

global Param;
global State;

counter = 0;
obs_counter = 0;

%This stores all of the hypotheses being evaluated as a queue.
cost_table = zeros(length(State.Ekf.mu(4:3:end))+1,2*length(z(1,:))+2);
%Table Format
%Hypothesis - Rows
%Cost, Match - Columns

%Initialize Table
%Populate table by generating all possible hypotheses for first observation
for counter=1:length(State.Ekf.mu(4:3:end))
    i = counter*3+1;
    cost_table(counter,1) = cost_function(z(:,1), i);
    cost_table(counter,2) = i;
end
cost_table(length(State.Ekf.mu(4:3:end))+1,1) = Param.ambi_dist;
cost_table(length(State.Ekf.mu(4:3:end))+1,2) = NaN;

%Queue Indices
front = 1; 
%Points to currently active element
back = length(State.Ekf.mu(4:3:end))+1+1;
%Points to first empty space

thresh = inf;
%Stores the value of the least expensive hypotheses

% Process till queue empties
while(front<back)
    %Extract element from queue
    el = cost_table(front,:);
    front = front + 1;
    
    obs_counter = 1;
    while obs_counter<=length(z(1,:)) && el(obs_counter*2) ~= 0 
        obs_counter = obs_counter+1;
    end

    % If hypothesis is more expensive than the known best hypothesis;
    % discard it from queue.
    if sum(el(1:2:end)) > thresh
        continue;
    end
    % Process base case
    if obs_counter==length(z(1,:))+1
        % If a full hypotheses costs is more expensive than a previously
        % seen full hypotheses, then ignore it.
        % If it cost less, then push it back into the queue.
        if sum(el(1:2:end)) < thresh
            thresh = sum(el(1:2:end));
            cost_table(back,:) = el;
            back = back + 1;
        end
        continue;
    end
    % Base case is a full hypotheses ie. all observations have been assigned
    % a matching landmark.
    % This selects the case having all assignments and the minimum cost.
    % Eventually, the queue contains only the minimum costing hypothese
    
    % If hypothese is not full, then process it.
    clear cost_table_temp
    cost_table_temp = [];
    % Generate hypotheses by appending cost, id pairs for the next
    % observation into the current hypotheses
    for counter=1:length(State.Ekf.mu(4:3:end))
        i = counter*3+1;
        if sum(i==el(2:2:end)) == 0
            cost_table_temp(i,1) = cost_function(z(:,obs_counter), i);
            cost_table_temp(i,2) = i;
        else
            cost_table_temp(i,1) = inf;
            cost_table_temp(i,2) = i;
        end
    end
    cost_table_temp((end + 1),1:2) = [Param.ambi_dist, NaN];
    
    % Out of all the possible matches, ignoring the previously matched
    % landmarks, select the least costing match. If no match is found, then
    % append the new landmark hypotheses.
    cost_table_temp(cost_table_temp(:,2)==0)=inf;
    [val, ind] = min(cost_table_temp(:,1));
    if exist('ind') && ~isnan(cost_table_temp(ind*2))
        if cost_table_temp(ind,1) < Param.min_dist
            el(obs_counter*2-1) = val;
            el(obs_counter*2) = cost_table_temp(ind,2);
        elseif cost_table_temp(ind,1) < Param.ambi_dist+0.4
             el(obs_counter*2) = -1;
             el(obs_counter*2-1) = Param.min_dist;
        end
    else
         el(obs_counter*2) = NaN;
         el(obs_counter*2-1) = Param.min_dist;
         
    end
    
    % Push newly evaluated hypothese back onto queue
    cost_table(back,:) = el;
    back = back + 1;
end

% Select the last element on the queue. This is the best JCBB hypothesis.
Li = cost_table(back-1, 2:2:(end-1));
Li(Li~=-1) = Li(Li~=-1) + 2;

function [cost] = cost_function(z_i, map_index)
    % Evaluate the cost of a match between an observation and a landmark.
    
    k = map_index; %First index (x co-ordinate)
    z_i(3) = 0;
    delta = [
           State.Ekf.mu(k) - State.Ekf.mu(1)
           State.Ekf.mu(k+1) - State.Ekf.mu(2)
           ];
    q = delta'*delta;

    zhat = [
           sqrt(q); 
           atan2(delta(2), delta(1))-State.Ekf.mu(3); 
           z_i(3)%State.Ekf.mu(k+2)% To set it to zero 
           % and not take it into consideration. 
           ];

    if strcmp(Param.choice, 'vp')
        zhat(2) = zhat(2) + 3.14/2;
    end
    zhat(2) = minimizedAngle(zhat(2));

    H_temp = (1/q)*[
           -sqrt(q)*delta(1), -sqrt(q)*delta(2), 0, ...
           sqrt(q)*delta(1), sqrt(q)*delta(2), 0;
           delta(2), -delta(1), -q, -delta(2), delta(1), 0;
           0, 0, 0, 0, 0, q;
           ];
    H = zeros(3, length(State.Ekf.mu));
    H(1:3,1:3) = H_temp(1:3,1:3);
    H(1:3,k:k+2) = H_temp(1:3,4:6);

    phi = H*State.Ekf.Sigma*H' + blkdiag(Param.R, 1e-4);
    cost = ((z_i-zhat)'*(phi^-1)*(z_i-zhat))^0.5; 
end
end