function Li = da_known(z)
% EKF-SLAM data association with known correspondences

global Param;
global State;

Li = [];
for counter = 1:length(z)
    % Find the index of an existing landmark with the same id
    % If no such landmark exists, then pass back a NaN, which leads to the
    % generation of a new landmark
    if isempty(find(State.Ekf.mu(6:3:end)==z(counter),1))
        Li = [Li;nan];
    else
        Li = [Li;3+3*find(State.Ekf.mu(6:3:end)==z(counter),1)];
    end
end

end