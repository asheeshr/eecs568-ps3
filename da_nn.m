function Li = da_nn(z, ~)
% perform nearest-neighbor data association

global Param;
global State;

Li = zeros(1, length(z(1,:)));
% Augment measurements to full size
z(3,:) = 0;

for counter = 1:length(z(1,:))
    pi = zeros(1, length(State.Ekf.mu(4:3:end))+1);
    
    % Iterate over all landmarks in map
    for i=1:length(State.Ekf.mu(4:3:end))    
        k = i*3+1;
        % Distance calculation of existing landmark versus robot pose
        delta = [
               State.Ekf.mu(k) - State.Ekf.mu(1)
               State.Ekf.mu(k+1) - State.Ekf.mu(2)
               ];
        q = delta'*delta;
        
        % Expected observation for landmark 
        zhat = [
               sqrt(q); 
               atan2(delta(2), delta(1))-State.Ekf.mu(3); 
               z(3,counter)%State.Ekf.mu(k+2)% To set it to zero 
               % and not take it into consideration. 
               ];
        
        if strcmp(Param.choice, 'vp')
            zhat(2) = zhat(2) + 3.14/2;
        end
        
        zhat(2) = minimizedAngle(zhat(2));

        % Calculating and extending Jacobian
        H_temp = (1/q)*[
               -sqrt(q)*delta(1), -sqrt(q)*delta(2), 0, ...
               sqrt(q)*delta(1), sqrt(q)*delta(2), 0;
               delta(2), -delta(1), -q, -delta(2), delta(1), 0;
               0, 0, 0, 0, 0, q;
               ];
        H = zeros(3, length(State.Ekf.mu));
        H(1:3,1:3) = H_temp(1:3,1:3);
        H(1:3,k:k+2) = H_temp(1:3,4:6);
        
        % Mahalanobis Distance Calculation
        phi = H*State.Ekf.Sigma*H' + blkdiag(Param.R, 1e-4);
        pi(i) = ((z(:,counter)-zhat)'*(phi^-1)*(z(:,counter)-zhat))^0.5;
    end
    pi(end) = Param.ambi_dist;
    
    % Find minimum distance element
    [val, ind] = min(pi);
    if  val >= Param.ambi_dist
        % If too far to be an existing or ambiguous landmark
        % New landmark
        Li(counter) = nan;
    elseif val > Param.min_dist
        % Ambiguous Maesurement. Ignoring.           
        Li(counter) = -1;
    else   
        % Accurate Match. Pass back index to landmark.
        Li(counter) = ind*3+1+2;
    end

end

end