function [] = ekfpredict_sim(u)
% EKF-SLAM prediction for simulator process model
global Param;
global State;

% Jacobian of motion update versus control input
J = [
    -u(2)*sin(State.Ekf.mu(3)+u(1)),  cos(State.Ekf.mu(3)+u(1)), 0;
    u(2)*cos(State.Ekf.mu(3)+u(1)),  sin(State.Ekf.mu(3)+u(1)), 0;
    1,                          0,                   1];

% Noise in motion model
sigma_motion = [
    Param.alphas(1)*u(1)^2 + Param.alphas(2)*u(2)^2, 0, 0;
    0, Param.alphas(4)*u(1)^2 + Param.alphas(4)*u(3)^2 + Param.alphas(3)*u(2)^2, 0;
    0, 0, Param.alphas(1)*u(3)^2 + Param.alphas(2)*u(2)^2];

% Jacobian of motion update versus state
G = [
    1, 0, -u(2)*sin(State.Ekf.mu(3)+u(1));
    0, 1,  u(2)*cos(State.Ekf.mu(3)+u(1));
    0, 0,  1];
 

%Update first three rows of mu
State.Ekf.mu(1:3) = prediction(State.Ekf.mu(1:3),u); %Handles angle rollover

% Update sigma piecewise 
State.Ekf.Sigma(1:3,1:3)= G*State.Ekf.Sigma(1:3,1:3)*G' + J*sigma_motion*J';
State.Ekf.Sigma(4:end,1:3) = State.Ekf.Sigma(4:end,1:3)*G';
State.Ekf.Sigma(1:3,4:end) = G*State.Ekf.Sigma(1:3,4:end);

end