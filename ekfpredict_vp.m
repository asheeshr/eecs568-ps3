function ekfpredict_vp(u, dt)
% EKF-SLAM prediction for Victoria Park process model

global Param;
global State;

% Vc Ve equation
u(1) = u(1)/(1 - tan(u(2))*Param.H/Param.L);

% Jacobian
G = [
    1, 0, dt*(u(1)*sin(State.Ekf.mu(3)) + ...
    u(1)*tan(u(2))*(Param.a*cos(State.Ekf.mu(3))- ...
    Param.b*sin(State.Ekf.mu(3)))/Param.L);
    
    0, 1, dt*(u(1)*cos(State.Ekf.mu(3)) - ...
    u(1)*tan(u(2))*(Param.a*sin(State.Ekf.mu(3))+ ...
    Param.b*cos(State.Ekf.mu(3)))/Param.L);
    
    0, 0,  1];
 
%Update first three rows of mu
mu(1:3) = [
    State.Ekf.mu(1) + dt*u(1)*(cos(State.Ekf.mu(3)) - ...
    tan(u(2))*(Param.a*sin(State.Ekf.mu(3))+ ...
    Param.b*cos(State.Ekf.mu(3)))/Param.L);
    
    State.Ekf.mu(2) + dt*u(1)*(sin(State.Ekf.mu(3)) + ...
    tan(u(2))*(Param.a*cos(State.Ekf.mu(3))- ...
    Param.b*sin(State.Ekf.mu(3)))/Param.L);
    
    State.Ekf.mu(3) + dt*u(1)*tan(u(2)/Param.L)
    ];

% Update mu
State.Ekf.mu(1:3) = mu;
State.Ekf.mu(3) = minimizedAngle(State.Ekf.mu(3));

% Update sigma piecewise
State.Ekf.Sigma(1:3,1:3)= G*State.Ekf.Sigma(1:3,1:3)*G' + Param.Qf;
State.Ekf.Sigma(4:end,1:3) = State.Ekf.Sigma(4:end,1:3)*G';
State.Ekf.Sigma(1:3,4:end) = G*State.Ekf.Sigma(1:3,4:end);


end 