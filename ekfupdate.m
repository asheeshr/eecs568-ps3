function [Li_full] = ekfupdate(z)
% EKF-SLAM update step for both simulator and Victoria Park data set

global Param;
global State;

% returns state vector indices pairing observations with landmarks
switch lower(Param.dataAssociation)
    case 'known'
        Li = da_known(z(3,:));
    case 'nn'
        Li = da_nn(z(1:2,:), Param.R);
    case 'jcbb'
        Li = da_jcbb(z(1:2,:), Param.R);
    otherwise
        error('unrecognized data association method: "%s"', ...
            Param.dataAssociation);
end

% Returning matches for plotting
Li_full = zeros(1,length(Li));
for i=1:length(Li)
    if isnan(Li(i)) || Li(i)==-1 
        Li_full(i) = nan;
    else
        Li_full(i) = State.Ekf.mu(Li(i));
    end
end


% Removing ambiguous measurements. 
z = z(:,(Li~=-1));
Li = Li(Li~=(-1));

switch Param.updateMethod
    case 'batch'
        % Allocate
        H = zeros(3*length(z(1,:)), length(State.Ekf.mu));
        Q = [];
        zhat = zeros(3*length(z(1,:)), 1);
        
        % Iterate over observations
        for counter = 1:length(z(1,:))
           
           i = Li(counter);
           if isempty(i) || isnan(i) % Checking if new
               initialize_new_landmark(z(:,counter), []);
               i = length(State.Ekf.mu);
           end
           
           % Distance calculation between observation and robot position
           delta = [
               State.Ekf.mu(i-2) - State.Ekf.mu(1)
               State.Ekf.mu(i-1) - State.Ekf.mu(2)
               ];
           q = delta'*delta;

           % Estimating expected observation
           zhat(counter*3-2:counter*3) = [
               sqrt(q); 
               atan2(delta(2), delta(1))-State.Ekf.mu(3); 
               State.Ekf.mu(i)
               ];
           
           % Angle adjustment for VP dataset
           if strcmp(Param.choice, 'vp')
            zhat(counter*3-1) = zhat(counter*3-1) + 3.14/2;
           end
           zhat(counter*3-1) = minimizedAngle(zhat(counter*3-1));
           
           % Jacobian of observation function versus state and landmark
           H_temp = (1/q)*[
               -sqrt(q)*delta(1), -sqrt(q)*delta(2), 0, ...
               sqrt(q)*delta(1), sqrt(q)*delta(2), 0;
               delta(2), -delta(1), -q, -delta(2), delta(1), 0;
               0, 0, 0, 0, 0, q;
               ];
           % Extending to full size
           H(counter*3-2:counter*3,1:3) = H_temp(1:3,1:3);
           H(counter*3-2:counter*3,i-2:i) = H_temp(1:3,4:6);
           % Forming noise matrix
           Q = blkdiag(Q, blkdiag(Param.R,1e-4));
        end
        % Stacking observations
        z = z(:);
        
        % Standard Kalman Filter Measurement Equations
        K = State.Ekf.Sigma*H'/(H*State.Ekf.Sigma*H' + Q);
        State.Ekf.mu = State.Ekf.mu + K*(z - zhat);
        State.Ekf.mu(3) = minimizedAngle(State.Ekf.mu(3));
        State.Ekf.Sigma = (eye(length(State.Ekf.mu)) - K*H)*State.Ekf.Sigma;
        
    case 'seq'
        for counter = 1:length(z(1,:))
           
           i = Li(counter);
           if isempty(i) || isnan(i)
               initialize_new_landmark(z(:,counter), []);
               i = length(State.Ekf.mu);
           end
           
           delta = [
               State.Ekf.mu(i-2) - State.Ekf.mu(1)
               State.Ekf.mu(i-1) - State.Ekf.mu(2)
               ];
           q = delta'*delta;

           zhat = [
               sqrt(q); 
               atan2(delta(2), delta(1))-State.Ekf.mu(3); 
               z(3,counter);
               ];
           
           % Angle adjustment for VP
           if strcmp(Param.choice, 'vp')
            zhat(2) = zhat(2) + 3.14/2;
           end
           
           zhat(2) = minimizedAngle(zhat(2));

           % Calculating Jacobian
           H_temp = (1/q)*[
               -sqrt(q)*delta(1), -sqrt(q)*delta(2), 0, ...
               sqrt(q)*delta(1), sqrt(q)*delta(2), 0;
               delta(2), -delta(1), -q, -delta(2), delta(1), 0;
               0, 0, 0, 0, 0, q;
               ];
           % Extending H to full size
           H = zeros(3, length(State.Ekf.mu));
           H(1:3,1:3) = H_temp(1:3,1:3);
           H(1:3,i-2:i) = H_temp(1:3,4:6);

           % Standard Kalman Filter Measurement Equations
           K = State.Ekf.Sigma*H'/(H*State.Ekf.Sigma*H' + blkdiag(Param.R,1e-4));
           State.Ekf.mu = State.Ekf.mu + K*(z(1:3, counter) - zhat);
           State.Ekf.mu(3) = minimizedAngle(State.Ekf.mu(3));
           State.Ekf.Sigma = (eye(length(State.Ekf.mu)) - K*H)*State.Ekf.Sigma;
        end
end
end