function initialize_new_landmark(z_i, R)

global Param;
global State;

% Augment state for VP
if strcmp(Param.choice, 'vp')
    mu = [
        State.Ekf.mu(1) + z_i(1)*cos(z_i(2)+State.Ekf.mu(3)-3.14/2);
        State.Ekf.mu(2) + z_i(1)*sin(z_i(2)+State.Ekf.mu(3)-3.14/2);
        z_i(3)
        ];
end

% Augment state for Sim
if strcmp(Param.choice, 'sim')
    mu = [
        State.Ekf.mu(1) + z_i(1)*cos(z_i(2)+State.Ekf.mu(3));
        State.Ekf.mu(2) + z_i(1)*sin(z_i(2)+State.Ekf.mu(3));
        z_i(3)
        ];    
end

State.Ekf.mu = [State.Ekf.mu; mu];
State.Ekf.Sigma = blkdiag(State.Ekf.Sigma, diag([1e8 1e8 1e8]));

end