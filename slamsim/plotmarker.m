function h = plotmarker( markerPos, color)

plot( markerPos(1), markerPos(2), 'x', 'linewidth', 2, 'Color', ...
      color);

