function varargout = runsim(stepsOrData, pauseLen, maxObs, nLandmarksPerSide, ...
    plotsEnabled)
set(0,'DefaultFigureWindowStyle','docked')

global Param;
global Data;
global State;

% For plots
global Ekf

if ~exist('pauseLen','var')
    pauseLen = 0.3; % seconds
end

if ~exist('plotsEnabled','var')
    plotsEnabled = true; % seconds
end

if ~exist('Param.choice', 'var')
    Param.choice = 'sim'
end

makeVideo = true;

GLOBAL_FIGURE = 1;

if makeVideo
    try
        votype = 'avifile';
        vo = avifile('video.avi', 'fps', min(5, 1/pauseLen));
    catch
        votype = 'VideoWriter';
        vo = VideoWriter('video', 'MPEG-4');
        set(vo, 'FrameRate', min(5, 1/pauseLen));
        open(vo);
    end
end


% Initalize Params
%===================================================
Param.initialStateMean = [180 50 0]';

% max number of landmark observations per timestep
if ~exist('maxObs','var')
    Param.maxObs = 2;
else
    Param.maxObs = maxObs;
end

% number of landmarks per sideline of field (minimum is 3)
if ~exist('nLandmarksPerSide','var')
    Param.nLandmarksPerSide = 4;
else
    Param.nLandmarksPerSide = nLandmarksPerSide;
end

% Motion noise (in odometry space, see p.134 in book).
Param.alphas = [0.05 0.001 0.05 0.01].^2; % std of noise proportional to alphas

% Standard deviation of Gaussian sensor noise (independent of distance)
Param.beta = [10, deg2rad(10)]; % [cm, rad]
Param.R = diag(Param.beta.^2);

% Step size between filter updates, can be less than 1.
Param.deltaT=0.1; % [s]

if isscalar(stepsOrData)
    % Generate a data set of motion and sensor info consistent with
    % noise models.
    numSteps = stepsOrData;
    Data = generateScript(Param.initialStateMean, numSteps, Param.maxObs, Param.alphas, Param.beta, Param.deltaT);
else
    % use a user supplied data set from a previous run
    Data = stepsOrData;
    numSteps = size(Data, 1);
    global FIELDINFO;
    FIELDINFO = getfieldinfo;
end

Param.min_dist = 3;
Param.ambi_dist = 4;
%===================================================

% Initialize State
%===================================================
State.Ekf.mu = Param.initialStateMean;
State.Ekf.Sigma = zeros(3);


% For plots
Ekf.li_hist = [];%zeros(numSteps,Param.maxObs+1);
Ekf.mu_path = [];
Ekf.det_hist = zeros(Param.nLandmarksPerSide*2, numSteps);

for t = 1:numSteps
    plotsim(t);

    %=================================================
    % data available to your filter at this time step
    %=================================================
    u = getControl(t);
    z = getObservations(t);


    %=================================================
    %TODO: update your filter here based upon the
    %      motionCommand and observation
    %=================================================
    ekfpredict_sim(u);
    
    Ekf.mu_path = [Ekf.mu_path, State.Ekf.mu(1:2)];

    Li = ekfupdate(z);
    
    
    %=================================================
    %TODO: plot and evaluate filter results here
    %=================================================
    
    % Storing Landmark indices for plots
    if length(Li)<Param.maxObs
        Li(end + Param.maxObs-length(Li)) = 0;
    end
    Ekf.li_hist = [Ekf.li_hist; t, Li];
    
    % Calculating det
    for i = 4:3:length(State.Ekf.mu)
        Ekf.det_hist(floor(i/3),t) = det(State.Ekf.Sigma(i:i+1,i:i+1))^0.25;
    end
     
    
    if plotsEnabled
        
    plotrobot(State.Ekf.mu(1), State.Ekf.mu(2), State.Ekf.mu(3), ...
         'cyan', 0, 'cyan');
    plotcov2d(State.Ekf.mu(1), State.Ekf.mu(2), State.Ekf.Sigma(1:3,1:3), ...
         'yellow', 1, 'yellow', 0.4, 3);
    for i = 1:length(State.Ekf.mu(4:3:end))
        plotcov2d(State.Ekf.mu(3*i+1), State.Ekf.mu(3*i+2), State.Ekf.Sigma(3*i+1:3*i+2,3*i+1:3*i+2), ...
         'red', 1, 'red', 0.4, 3);
    end
    drawnow;
    if pauseLen > 0
        pause(pauseLen);
    end
    end
    
    if makeVideo
        F = getframe(gcf);
        switch votype
          case 'avifile'
            vo = addframe(vo, F);
          case 'VideoWriter'
            writeVideo(vo, F);
          otherwise
            error('unrecognized votype');
        end
    end
end


if makeVideo
    fprintf('Writing video...');
    switch votype
      case 'avifile'
        vo = close(vo);
      case 'VideoWriter'
        close(vo);
      otherwise
        error('unrecognized votype');
    end
    fprintf('done\n');
end

str = sprintf('../../figures/task2-3sigma-%d',numSteps);
print(str,'-dpng')

% Generate final set of plots
generate_plots()

disp('Landmarks'); disp(length(State.Ekf.mu(4:3:end)));

% Calculate landmark errors
li_errors = [];
for i=1:Param.maxObs
        li_errors = [li_errors; 
            reshape(Data.Sim.noisefreeObservation(3,i,:), ...
            [length(Data.Sim.noisefreeObservation(3,i,:)),1])-Ekf.li_hist(:,i+1)];
end

if nargout >= 1
    varargout{1} = Data;
    varargout{2} = nnz(li_errors~=0);
end

%==========================================================================
function u = getControl(t)
global Data;
% noisefree control command
u = Data.noisefreeControl(:,t);  % 3x1 [drot1; dtrans; drot2]


%==========================================================================
function z = getObservations(t)
global Data;
% noisy observations
z = Data.realObservation(:,:,t); % 3xn [range; bearing; landmark id]
ii = find(~isnan(z(1,:)));
z = z(:,ii);

%==========================================================================
function plotsim(t)
global Data;

%--------------------------------------------------------------
% Graphics
%--------------------------------------------------------------

NOISEFREE_PATH_COL = 'green';
ACTUAL_PATH_COL = 'blue';

NOISEFREE_BEARING_COLOR = 'cyan';
OBSERVED_BEARING_COLOR = 'red';

GLOBAL_FIGURE = 1;

%=================================================
% data *not* available to your filter, i.e., known
% only by the simulator, useful for making error plots
%=================================================
% actual position (i.e., ground truth)
x = Data.Sim.realRobot(1,t);
y = Data.Sim.realRobot(2,t);
theta = Data.Sim.realRobot(3,t);

% real observation
observation = Data.realObservation(:,:,t);

% noisefree observation
noisefreeObservation = Data.Sim.noisefreeObservation(:,:,t);

%=================================================
% graphics
%=================================================
figure(GLOBAL_FIGURE); clf; hold on; plotfield(observation(3,:));

% draw actual path (i.e., ground truth)
plot(Data.Sim.realRobot(1,1:t), Data.Sim.realRobot(2,1:t), 'Color', ACTUAL_PATH_COL);
plotrobot( x, y, theta, 'black', 1, ACTUAL_PATH_COL);

% draw noise free motion command path
plot(Data.Sim.noisefreeRobot(1,1:t), Data.Sim.noisefreeRobot(2,1:t), 'Color', NOISEFREE_PATH_COL);
plot(Data.Sim.noisefreeRobot(1,t), Data.Sim.noisefreeRobot(2,t), '*', 'Color', NOISEFREE_PATH_COL);

for k=1:size(observation,2)
    rng = Data.Sim.noisefreeObservation(1,k,t);
    ang = Data.Sim.noisefreeObservation(2,k,t);
    noisy_rng = observation(1,k);
    noisy_ang = observation(2,k);

    % indicate observed range and angle relative to actual position
    plot([x x+cos(theta+noisy_ang)*noisy_rng], [y y+sin(theta+noisy_ang)*noisy_rng], 'Color', OBSERVED_BEARING_COLOR);

    % indicate ideal noise-free range and angle relative to actual position
    plot([x x+cos(theta+ang)*rng], [y y+sin(theta+ang)*rng], 'Color', NOISEFREE_BEARING_COLOR);
end

function generate_plots()

    global Param;
    global Data;
    global State;
    % For plots
    global Ekf

%     figure(2)
%     clf;
%     hold on
%     for i = 1:length(State.Ekf.mu(4:3:end))
%         plotcov2d(State.Ekf.mu(3*i+1), State.Ekf.mu(3*i+2), ...
%             State.Ekf.Sigma(3*i+1:3*i+2,3*i+1:3*i+2), ...
%          'red', 1, 'red', 0.4, 3);
%     end
%     plot(Ekf.mu_path(1,:), Ekf.mu_path(2,:));
%     
%     str = sprintf('../../figures/task2-3sigma-%d',length(Ekf.mu_path(1,:)));
%     print(str,'-dpng')
%     hold off
%     
    figure(3)
    clf;
    hold on
    
    s_actual = [];
    s_estimated = []; 
    for i=1:Param.maxObs
        s_actual = [s_actual; ...
            scatter(1:length(Data.Sim.noisefreeObservation(3,i,:)), ...
            Data.Sim.noisefreeObservation(3,i,:))];
        s_actual(i).MarkerEdgeColor = s_actual(i).CData;
        s_estimated = [s_estimated; ...
            scatter(1:length(Ekf.li_hist(:,i+1)), Ekf.li_hist(:,i+1), '+', ...
            'MarkerEdgeColor', s_actual(i).MarkerEdgeColor)
        ];
        s_estimated(i).MarkerEdgeColor = s_estimated(i).CData;
        
    end
    legend([s_actual(1), s_estimated(1)], {'Real Id', ...
        'NN Estimate'})
    xlabel('Time (steps)')
    ylabel('Landmark Id')
    str = sprintf('Landmark Ids versus Nearest Neighbour Matches %d Steps', ...
        length(Ekf.mu_path(1,:)));
    title(str)
    str = sprintf('../../figures/task2-error-%d-maxObs-%d-nland-%d', ...
        length(Ekf.mu_path(1,:)),Param.maxObs, Param.nLandmarksPerSide);
    print(str,'-dpng')
    hold off

    
    figure(4)
    clf;
    hold on
    
    s_actual = [];
    s_estimated = []; 
    for i=1:Param.maxObs
        s_actual = [s_actual; ...
            scatter(1:length(Data.Sim.noisefreeObservation(3,i,:)), ...
            fix(reshape(Data.Sim.noisefreeObservation(3,i,:), ...
            [length(Data.Sim.noisefreeObservation(3,i,:)),1])-Ekf.li_hist(:,i+1)))];
        s_actual(i).MarkerEdgeColor = s_actual(i).CData;
%         s_estimated = [s_estimated; ...
%             scatter(1:length(Ekf.li_hist(:,i+1)), Ekf.li_hist(:,i+1), '+', ...
%             'MarkerEdgeColor', s_actual(i).MarkerEdgeColor)
%         ];
%         s_estimated(i).MarkerEdgeColor = s_estimated(i).CData;
        
    end
    %legend([s_actual(1)], {'Error'})
    xlabel('Time (steps)')
    ylabel('Landmark Id Error')
    str = sprintf('Landmark Id Errors %d Steps', ...
        length(Ekf.mu_path(1,:)));
    title(str)
    str = sprintf('../../figures/task2-error0-%d-maxObs-%d-nland-%d',...
        length(Ekf.mu_path(1,:)), Param.maxObs, Param.nLandmarksPerSide);
    print(str,'-dpng')
    hold off
    
    figure(5)
    clf;
    hold on
    
    corr_coeff = State.Ekf.Sigma;
    sigmas = sqrt(State.Ekf.Sigma(logical(eye(size(State.Ekf.Sigma)))));
    corr_coeff = corr_coeff./repmat(sigmas, 1, length(State.Ekf.mu));
    corr_coeff = corr_coeff./repmat(sigmas', length(State.Ekf.mu), 1);
    p = [];
    for i=4:3:length(State.Ekf.mu)
        p = [p,plot(1:length(State.Ekf.mu(4:3:end)), corr_coeff(4:3:end, i))];
        
    end
    
    xlabel('Landmark Id')
    ylabel('Correlation Coefficient')
    legend_entries = cellstr(num2str(1:length(State.Ekf.mu(4:3:end)), ...
        'Id=%-d'));
    legend(p, cellstr(num2str([1:length(State.Ekf.mu(4:3:end))]')), ...
        'Location', 'northoutside', 'Orientation', 'horizontal')
    str = sprintf('Correlation Coefficient for Landmarks for %d Steps', ...
        length(Ekf.mu_path(1,:)));
    title(str)
    str = sprintf('../../figures/task2-rholm-%d-maxObs-%d-nland-%d',...
        length(Ekf.mu_path(1,:)), Param.maxObs, Param.nLandmarksPerSide);
    print(str,'-dpng')
    hold off
    
    figure(6)
    clf;
    hold on
    
    plot(1:length(State.Ekf.mu(1:3:end)), corr_coeff(1:3:end, 1));
    plot(1:length(State.Ekf.mu(2:3:end)), corr_coeff(2:3:end, 1));
    
    xlabel('Landmark Id')
    ylabel('Correlation Coefficient')
    legend('X Coefficients', 'Y Coefficients');
    str = sprintf('Robot X and Y Coefficient wrt Landmarks for %d Steps', ...
        length(Ekf.mu_path(1,:)));
    title(str)
    str = sprintf('../../figures/task2-rhorob-%d-maxObs-%d-nland-%d',...
        length(Ekf.mu_path(1,:)), Param.maxObs, Param.nLandmarksPerSide);
    print(str,'-dpng')
    hold off
    
    figure(7)
    clf;
    hold on
    
    i1 = ones(1,length(State.Ekf.mu));
    i2 = ones(1,length(State.Ekf.mu));
    i1(6:3:length(State.Ekf.mu)) = 0; %repmat(1,1,length(State.Ekf.mu(1:3:end)));
    i2(6:3:length(State.Ekf.mu)) = 0; %repmat(1,1,length(State.Ekf.mu(1:3:end)));
    i1 = logical(i1);
    i2 = logical(i2);
    
    imshow(mat2gray(1-corr_coeff(i1,i2)))
    str = sprintf('Covariance Coefficients %d Steps', ...
        length(Ekf.mu_path(1,:)));
    title(str)
    str = sprintf('../../figures/task2-rhoimg-%d-maxObs-%d-nland-%d',...
        length(Ekf.mu_path(1,:)), Param.maxObs, Param.nLandmarksPerSide);
    print(str,'-dpng')
    hold off
    
    figure(8)
    clf;
    hold on
    
    det_array = [];
    for i = 4:3:length(State.Ekf.mu)
        det_array = [det_array, 
        det(State.Ekf.Sigma(i:i+1,i:i+1))^0.25    
        ];
    end
    
    plot(1:length(State.Ekf.mu(4:3:end)), det_array);
    scatter(1:length(State.Ekf.mu(4:3:end)), det_array, 'blue');
    xlabel('Landmark Id')
    ylabel('Determinant of Feature Covariance ^ 1/4')
    str = sprintf('Determinant of Landmark Covariance Matrices for %d Steps', ...
        length(Ekf.mu_path(1,:)));
    title(str)
    str = sprintf('../../figures/task2-rhodet-%d-maxObs-%d-nland-%d',...
        length(Ekf.mu_path(1,:)), Param.maxObs, Param.nLandmarksPerSide);
    print(str,'-dpng')
    hold off
    
    
    figure(9)
    clf;
    hold on
    p=[];
    for i=4:3:length(State.Ekf.mu)
        p = [p, plot(1:length(Ekf.det_hist(1,:)), Ekf.det_hist(floor(i/3),:))];
    end
    xlabel('Time')
    ylabel('Determinant of Feature Covariance ^ 1/4')
    legend_entries = cellstr(num2str(1:length(State.Ekf.mu(4:3:end)), ...
        'Id=%-d'));
    legend(p, cellstr(num2str([1:length(State.Ekf.mu(4:3:end))]')), ...
        'Location', 'northoutside', 'Orientation', 'horizontal')
    str = sprintf('Evolution of Determinant of Landmark Covariance Matrices for %d Steps', ...
        length(Ekf.mu_path(1,:)));
    title(str)
    str = sprintf('../../figures/task2-rhodettime-%d-maxObs-%d-nland-%d',...
        length(Ekf.mu_path(1,:)), Param.maxObs, Param.nLandmarksPerSide);
    print(str,'-dpng')
    hold off