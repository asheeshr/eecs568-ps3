function runvp(nSteps,pauseLen)
set(0,'DefaultFigureWindowStyle','docked')

global Param;
global State;
global Data;

if ~exist('nSteps','var') || isempty(nSteps)
    nSteps = inf;
end

if ~exist('pauseLen','var')
    pauseLen = 0; % seconds
end


if ~exist('Param.choice', 'var')
    Param.choice = 'vp'
end

makeVideo = false;

GLOBAL_FIGURE = 1;

if makeVideo
    try
        votype = 'avifile';
        vo = avifile('video.avi', 'fps', min(5, 1/pauseLen));
    catch
        votype = 'VideoWriter';
        vo = VideoWriter('video', 'MPEG-4');
        set(vo, 'FrameRate', min(5, 1/pauseLen));
        open(vo);
    end
end

Data = load_vp_si();

% Initalize Params
%===================================================
% vehicle geometry
Param.a = 3.78; % [m]
Param.b = 0.50; % [m]
Param.L = 2.83; % [m]
Param.H = 0.76; % [m]

% 2x2 process noise on control input
sigma.vc = 0.02; % [m/s]
sigma.alpha = 2*pi/180; % [rad]
Param.Qu = diag([sigma.vc, sigma.alpha].^2);

% 3x3 process noise on model error
sigma.x = 0.1; % [m]
sigma.y = 0.1; % [m]
sigma.phi = 0.5*pi/180; % [rad]
Param.Qf = diag([sigma.x, sigma.y, sigma.phi].^2);

% 2x2 observation noise
sigma.r = 0.05; % [m]
sigma.beta = 1*pi/180; % [rad]
Param.R = diag([sigma.r, sigma.beta].^2);

% Mahanalobis Values for gates
if strcmp(Param.choice, 'vp')
    Param.min_dist = 3;
    Param.ambi_dist = 4;
end
%===================================================

% Initialize State
%===================================================
State.Ekf.mu = [Data.Gps.x(2), Data.Gps.y(2), 36*pi/180]';
State.Ekf.Sigma = zeros(3);

global AAr;
AAr = [0:360]*pi/360;


figure(1); clf;
axis equal;

Ekf.mu_path = [];
Ekf.time_update = [];
Ekf.time_predict = [];
ci = 1; % control index
t = min(Data.Laser.time(1), Data.Control.time(1));
for k=1:min(nSteps, length(Data.Laser.time))
    
    while (Data.Control.time(ci) < Data.Laser.time(k))
       % control available
       dt = Data.Control.time(ci) - t;
       t = Data.Control.time(ci);
       u = [Data.Control.ve(ci), Data.Control.alpha(ci)]';
       
       tic
       ekfpredict_vp(u, dt);
       Ekf.time_predict = [Ekf.time_predict , ...
           [toc;length(State.Ekf.mu(4:3:end))]];
       ci = ci+1;
    end
    
    % observation available
    dt = Data.Laser.time(k) - t;
    t = Data.Laser.time(k);
    z = detectTreesI16(Data.Laser.ranges(k,:));

    tic
    ekfupdate(z);
    Ekf.time_update = [Ekf.time_update, ...
           [toc;length(State.Ekf.mu(4:3:end))]];
       
    
    Ekf.mu_path = [Ekf.mu_path, State.Ekf.mu(1:2)];
    
    doGraphics(z);
    drawnow;
    if pauseLen > 0
        pause(pauseLen);
    end
    
    if makeVideo
        F = getframe(gcf);
        switch votype
          case 'avifile'
            vo = addframe(vo, F);
          case 'VideoWriter'
            writeVideo(vo, F);
          otherwise
            error('unrecognized votype');
        end
    end

end


if makeVideo
    fprintf('Writing video...');
    switch votype
      case 'avifile'
        vo = close(vo);
      case 'VideoWriter'
        close(vo);
      otherwise
        error('unrecognized votype');
    end
    fprintf('done\n');
end

% Generates final plots
generate_plots()

disp('Landmarks'); disp(length(State.Ekf.mu(4:3:end)));


%==========================================================================
function doGraphics(z)
% Put whatever graphics you want here for visualization
%
% WARNING: this slows down your process time, so use sparingly when trying
% to crunch the whole data set!

%global Param;
%global State;

% plot the robot and 3-sigma covariance ellipsoid
plotbot(State.Ekf.mu(1), State.Ekf.mu(2), State.Ekf.mu(3), 'black', 1, 'blue', 1);
hold on;

plotcov2d( State.Ekf.mu(1), State.Ekf.mu(2), State.Ekf.Sigma, 'blue', 0, 'blue', 0, 3);

for i = 1:length(State.Ekf.mu(4:3:end))
        plotcov2d(State.Ekf.mu(3*i+1), State.Ekf.mu(3*i+2), ...
            State.Ekf.Sigma(3*i+1:3*i+2,3*i+1:3*i+2), ...
         'red', 1, 'red', 0.4, 3);
end

% restrict view to a bounding box around the current pose
%BB=15;
%axis([[-BB,BB]+State.Ekf.mu(1), [-BB,BB]+State.Ekf.mu(2)]);


% project raw sensor detections in global frame using estimate pose
xr = State.Ekf.mu(1);
yr = State.Ekf.mu(2);
tr = State.Ekf.mu(3);
for k=1:size(z,2)
    r = z(1,k);
    b = z(2,k);
    xl = xr + r*cos(b+tr-pi/2);
    yl = yr + r*sin(b+tr-pi/2);
    plot([xr; xl], [yr; yl],'r',xl,yl,'r*');
end

hold off;
end

function generate_plots()

    figure(2)
    clf;
    hold on
    for i = 1:length(State.Ekf.mu(4:3:end))
        plotcov2d(State.Ekf.mu(3*i+1), State.Ekf.mu(3*i+2), ...
            State.Ekf.Sigma(3*i+1:3*i+2,3*i+1:3*i+2), ...
         'red', 1, 'red', 0.4, 3);
    end
    plot(Ekf.mu_path(1,:), Ekf.mu_path(2,:));
    
    str = sprintf('../../figures/task3-3sigma-%d',length(Ekf.mu_path(1,:)));
    print(str,'-dpng')
    hold off
    
    figure(3)
    clf;
    hold on
    s1 = scatter(Ekf.time_predict(2,:), Ekf.time_predict(1,:), 'green');
    s2 = scatter(Ekf.time_update(2,:), Ekf.time_update(1,:), 'blue');
    % MATLAB Bug http://www.mathworks.com/support/bugreports/1283854
    s1.MarkerEdgeColor = s1.CData;
    s2.MarkerEdgeColor = s2.CData;
    
    legend([s1, s2],{'Predict Step', 'Update Step'})
    xlabel('Number of Landmarks')
    ylabel('Time (seconds)')
    str = sprintf('Time for Update and Prediction over %d Steps', ...
        length(Ekf.mu_path(1,:)));
    title(str)
    str = sprintf('../../figures/task3-time-%d',length(Ekf.mu_path(1,:)));
    print(str,'-dpng')
    hold off
end

end